//
//  DummyDataSource.swift
//  Timeline
//
//  Created by Duc Nguyen on 3/1/18.
//  Copyright © 2018 Duc Nguyen. All rights reserved.
//

import Foundation

struct Topic {

}
struct TimelineItem {
    let name: String
    let image: String
    let url: String
    let topics: [Topic]?
}

typealias TimelineDict = [TimelineItem]

class DummyDataSource {
    class func allTimeline() -> TimelineDict {
        return [
            TimelineItem(name: "abc", image: "", url: "", topics: [Topic(), Topic()]),
            TimelineItem(name: "abc", image: "", url: "", topics: [Topic()]),
            TimelineItem(name: "abc", image: "", url: "", topics: [Topic(), Topic()]),
            TimelineItem(name: "abc", image: "", url: "", topics: [Topic()]),
            TimelineItem(name: "abc", image: "", url: "", topics: [Topic(), Topic(), Topic()]),
            TimelineItem(name: "abc", image: "", url: "", topics: [Topic()]),
            TimelineItem(name: "abc", image: "", url: "", topics: [Topic()]),
            TimelineItem(name: "abc", image: "", url: "", topics: [Topic(), Topic()]),
            TimelineItem(name: "abc", image: "", url: "", topics: [Topic()]),
            TimelineItem(name: "abc", image: "", url: "", topics: [Topic(), Topic()])
        ]
    }
}
