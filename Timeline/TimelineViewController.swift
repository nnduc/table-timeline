//
//  TimelineViewController.swift
//  Timeline
//
//  Created by Duc Nguyen on 3/1/18.
//  Copyright © 2018 Duc Nguyen. All rights reserved.
//

import UIKit

class TimelineViewController: UITableViewController {

    private var timeline: TimelineDict = DummyDataSource.allTimeline()

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(TimelineCell.self)
//        tableView.estimatedRowHeight = 120
//        tableView.rowHeight = UITableViewAutomaticDimension
        clearsSelectionOnViewWillAppear = false
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return timeline.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: TimelineCell.self)) as! TimelineCell
        cell.timelineItem = timeline[indexPath.row]
        return cell
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return max(topicHeight * CGFloat(timeline[indexPath.row].topics?.count ?? 1 ), 60)
    }


    @IBAction func addMoreTimelineItem(_ sender: UIBarButtonItem) {
        timeline.insert(TimelineItem(name: "xxx", image: "", url: "", topics: [Topic()]), at: 0)
        tableView.reloadRows(at: [IndexPath(item: 0, section: 0)], with: .automatic)
    }
}
