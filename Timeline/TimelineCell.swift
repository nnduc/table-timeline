//
//  TimelineCell.swift
//  Timeline
//
//  Created by Duc Nguyen on 3/1/18.
//  Copyright © 2018 Duc Nguyen. All rights reserved.
//

import UIKit

let topicHeight: CGFloat = 80

extension Array {
    var isNotEmpty: Bool {
        return !isEmpty
    }
}

class TimelineCell: UITableViewCell {

    @IBOutlet weak var topicStack: UIView!


    var timelineItem: TimelineItem? {
        didSet {
            updateView()
        }
    }

    func updateView() {
        timelineItem?.topics.map {
            guard $0.isNotEmpty else { return }
            for index in 0 ..< $0.count {
                let detailView = DetailsView(frame: CGRect(x: 0, y: topicHeight * CGFloat(index), width: topicStack.frame.width, height: topicHeight))
                topicStack.addSubview(detailView)
            }

            let topicStackFrame = topicStack.frame

            topicStack.frame = CGRect(x: topicStackFrame.minX, y: topicStackFrame.minY, width: topicStackFrame.width, height: CGFloat($0.count) * topicHeight)
        }
    }
}
